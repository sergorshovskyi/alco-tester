from calculation.main_calc_logic import get_result, get_how_long_to_drive_car
from constants.constants import alc_dict_by_type, possible_genders, all_alco

alco = input('Введіть що ви вживали: ')

while alco not in all_alco:
    alco = input('Вибачте, наразі AlcoTester не має інформації про даний вид алкоголю. \nБудь ласка, введіть один з '
                 'наступних варіантів: пиво, сидр, вино, мартіні, лікер, наливка, самогон, водка: ')

amount = int(input('Введіть кількість вжитого алкоголю: '))
if alco in alc_dict_by_type['easy']:
    while amount > 5000:
        amount = int(input('Введіть кількість вжитого алкоголю(алкоголь даного виду обробляється до 5000 мл: '))

elif alco in alc_dict_by_type['medium']:
    while amount > 2000:
        amount = int(input('Введіть кількість вжитого алкоголю(алкоголь даного виду обробляється до 2000 мл: '))

elif alco in alc_dict_by_type['hard']:
    while amount > 1000:
        amount = int(input('Введіть кількість вжитого алкоголю(алкоголь даного виду обробляється до 1000 мл: '))

age = input('Введіть ваш вік: ')
while (not age.isdigit()) or int(age) < 16 or int(age) > 90:
    age = input(
        'Вік має бути в межах 16-90, якщо ви дійсно такого віку – зателефонуйте батькам/онукам. \nВведіть ваш вік: ')

gender = input('Введіть вашу стать (хлопець/дівчина): ')
while gender.lower() not in possible_genders:
    gender = input('Будь ласка, введіть вашу стать: дівчина/хлопець')

experience = input('Введіть число, яке відповідає тому, як часто ви вживаєте (1 - декілька разів в тиждень; 2 - '
                   'щотижня; 3 - декілька разів в місяць; 4 - декілька разів в рік, 5 - майже ніколи) : ')
while (not experience.isdigit()) or int(experience) > 5 or int(experience) < 1:
    experience = input('Будь ласка, введіть коректне значення: ')

meal = input('Введіть число, яке відповідає тому, як добре ви поїли перед застіллям (1 - я добре поїв(ла); 2 - я '
             'перекусив(ла); 3 - я нічого не їв(ла)) : ')
while (not meal.isdigit()) or int(meal) > 3 or int(meal) < 1:
    meal = input('Будь ласка, введіть коректне значення: ')

feeling = input('Ваше самопочуття? Можлива відповідь: 1 - добре, 2 - відчуваю в’ялість, 3 - відчуваю нудоту, '
                '4 -погано, 5 - все одразу: ')
while not feeling.isdigit() or int(feeling) > 5 or int(feeling) < 1:
    feeling = input('Будь ласка, введіть коректне значення: ')

print(get_result(alco, int(amount), int(age), gender.lower(), int(experience), int(meal), int(feeling)))
