AlcoTester (in progress)

[Вимоги](https://docs.google.com/document/d/1XmEntr0mn62Gz_GzFT0VjXMmg4z33CFkMDQasd1pgKg/edit?usp=sharing 
) - доступно для ЧНУ-аккаунтів

_calculation folder_ - для скриптів, що відповідають за логіку обрахунку по наданим коефіцієнтам


_constants_ - для незмінних даних, констант, файлів і тд.

_helpers_ - для будь-яких допоміжних ф-цій та методів(extract coeffs)

_main.py_ - точка запуску програми, файл повинен запускати тільки одну основну ф-цію і бути максимально лаконічним