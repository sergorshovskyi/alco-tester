from helpers.coef_extract import *


def get_result(alco, amount, age, gender, experience, meal, feeling):
    age_coef = get_year_coef(age)
    amount_coef = get_amount_coef(amount, alco)
    gender_coef = get_sex_coef(gender)
    alco_coef = get_alcohol_coef(alco)
    experience_coef = get_experience_coef(experience)
    meal_coef = get_meal_coef(meal)
    feeling_coef = get_feeling_coef(feeling)
    return calculate_state(age_coef, amount_coef, gender_coef, alco_coef, experience_coef, meal_coef, feeling_coef)


# arguments are coefficients extracted from get_coeffs function
def calculate_state(*args):
    default_coef = 1000
    for coef in args:
        default_coef *= coef
    return get_string_result(default_coef), get_how_long_to_drive_car(default_coef)


def get_string_result(coef):
    if coef < 8000:
        return 'Все нормально'
    elif 11000 >= coef > 8000:
        return 'Обережно'
    elif 13000 >= coef > 11000:
        return 'Пора закінчувати'
    elif coef > 13000:
        return 'УВАГА! ДОСИТЬ!'
    else:
        return 1


def get_how_long_to_drive_car(coef):
    if coef < 8000:
        return 'За кермо можна сідати приблизно через 3 години'
    elif 11000 >= coef > 8000:
        return 'За кермо можна сідати приблизно через 6 годин'
    elif 13000 >= coef > 11000:
        return 'За кермо можна сідати приблизно через 12 годин'
    elif coef > 13000:
        return 'Краще потерпіти одну сутку, і лиш тоді сідати за кермо'
    else:
        return 1
