from constants.constants import alc_dict_by_type
from constants.coefficient import *


def get_alcohol_coef(alcohol):
    if alcohol in alc_dict_by_type['easy']:
        return EASY_ALC
    elif alcohol in alc_dict_by_type['medium']:
        return MEDIUM_ALC
    elif alcohol in alc_dict_by_type['hard']:
        return HARD_ALC
    else:
        return 1


def get_year_coef(year):
    if year < 16:
        return 1
    elif year > 90:
        return 1
    elif 20 >= year > 16:
        return YEARS_LESS_20
    elif 35 >= year > 20:
        return YEARS_LESS_35
    elif 55 >= year > 35:
        return YEARS_LESS_55
    elif 90 >= year > 55:
        return YEARS_LESS_90
    else:
        return 1

def get_sex_coef(sex):
    if sex == 'дівчина':
        return FEMALE
    elif sex == 'хлопець':
        return MALE
    else:
        return 1

def get_feeling_coef(feeling):
    if (feeling == 1):
        return GOOD;
    elif feeling == 2:
        return LETHARGIC
    elif feeling == 3:
        return NAUSEOUS
    elif feeling == 4:
        return BAD
    elif feeling == 5:
        return ALL_AT_ONCE
    else:
        return 1

def get_experience_coef(experience):
    if experience == 1:
        return ALMOST_EVERYDAY
    elif experience == 2:
        return ONCE_A_WEEK
    elif experience == 3:
        return SEVERAL_TIMES_A_MONTH
    elif experience == 4:
        return SEVERAL_TIMES_A_YEAR
    elif experience == 5:
        return ALMOST_NEVER
    else:
        return 1

def get_meal_coef(meal):
    if meal == 1:
        return ATE_WELL
    elif meal == 2:
        return SNACK
    elif meal == 3:
        return NOTHING
    else:
        return 1


def get_amount_coef(amount, alco):
    if alco in alc_dict_by_type['easy']:
        if amount > 0 and amount < 500:
            return EasyConsumed.less_500.value
        elif amount >= 500 and amount < 1000:
            return EasyConsumed.less_1000.value
        elif amount >=1000 and amount < 3000:
            return EasyConsumed.less_3000.value
        elif amount >=3000 and amount < 5000:
            return EasyConsumed.less_5000.value
        else:
            return 1
    elif alco in alc_dict_by_type['medium']:
        if amount > 0 and amount < 400:
            return MediumConsumed.less_400.value
        elif amount >= 400 and amount < 700:
            return MediumConsumed.less_7000.value
        elif amount >=700 and amount < 1000:
            return MediumConsumed.less_1000.value
        elif amount >=1000 and amount < 1500:
            return MediumConsumed.less_1500.value
        elif amount >=1500 and amount < 2000:
            return MediumConsumed.less_2000.value
        else:
            return 1
    elif alco in alc_dict_by_type['hard']:
        if amount > 0 and amount < 50:
            return HardConsumed.less_50.value
        elif amount >= 50 and amount < 100:
            return HardConsumed.less_100.value
        elif amount >=100 and amount < 200:
            return HardConsumed.less_200.value
        elif amount >=200 and amount < 600:
            return HardConsumed.less_600.value
        elif amount >=600 and amount < 1000:
            return HardConsumed.less_1000.value
        else:
            return 1

